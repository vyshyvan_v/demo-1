import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PassChangingModalComponent } from './pass-changing-modal.component';

describe('PassChangingModalComponent', () => {
  let component: PassChangingModalComponent;
  let fixture: ComponentFixture<PassChangingModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PassChangingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassChangingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
