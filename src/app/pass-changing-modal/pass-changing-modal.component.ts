import { Component, OnInit } from '@angular/core';
import { PostAddService } from '../post-add.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-pass-changing-modal',
  templateUrl: './pass-changing-modal.component.html',
  styleUrls: ['./pass-changing-modal.component.css']
})
export class PassChangingModalComponent implements OnInit {

  passwordToAsteriks;
  constructor(private http: PostAddService) { }

  ngOnInit() {
  }

  changePassMethod(v) {
    this.http.userPassword = v.new_pass;
    this.passwordToAsteriks = this.http.userPassword.split("");
    for(let i=0; i<this.http.userPassword.length; i++){
      this.passwordToAsteriks[i] = "*";
    }
    this.passwordToAsteriks = this.passwordToAsteriks.join("");
    this.http.userPassword = this.passwordToAsteriks;
    this.changePass(v.new_pass);
    this.http.swModal2 = false;
  }

  changePass(v) {
    var user = firebase.auth().currentUser;
    user.updatePassword(v);
  }

}
