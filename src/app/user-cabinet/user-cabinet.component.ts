import { PostAddService } from './../post-add.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-cabinet',
  templateUrl: './user-cabinet.component.html',
  styleUrls: ['./user-cabinet.component.css']
})
export class UserCabinetComponent implements OnInit {

  constructor(private service: PostAddService) { }

  ngOnInit() {
  }
  
  showModal1(){
    this.service.swModal1 = true;
  }

  showModal2(){
    this.service.swModal2 = true;
  }
}
