import { Component, OnInit } from '@angular/core';
import { PostAddService } from '../post-add.service';
import { HttpBackend } from '@angular/common/http';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  flag = false;
  constructor(private http: PostAddService) { }

  ngOnInit() {
  }

  logout(){
    console.log("email reset");
    this.http.userEmail = '';
    console.log(this.http.userEmail);
    this.http.flag = false;
  }

  onHomePage() {
    this.flag = true;
  }

  onUserPage() {
    this.flag = false;
  }

}
