import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { CarouselComponent } from './carousel/carousel.component';
import { FooterComponent } from './footer/footer.component';
import { PostListComponent } from './post-list/post-list.component';
import { SingInModalComponent } from './sing-in-modal/sing-in-modal.component';
import { SignUpModalComponent } from './sign-up-modal/sign-up-modal.component';
import { CreatePostModalComponent } from './create-post-modal/create-post-modal.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { PostAddService } from './post-add.service';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UserCabinetComponent } from './user-cabinet/user-cabinet.component';
import { SignedInPostListComponent } from './signed-in-post-list/signed-in-post-list.component';
import { AppRoutingModule } from './app-routing.module';
import { EmailChangingModalComponent } from './email-changing-modal/email-changing-modal.component';
import { PassChangingModalComponent } from './pass-changing-modal/pass-changing-modal.component'

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainMenuComponent,
    CarouselComponent,
    FooterComponent,
    PostListComponent,
    SingInModalComponent,
    SignUpModalComponent,
    CreatePostModalComponent,
    UserCabinetComponent,
    SignedInPostListComponent,
    EmailChangingModalComponent,
    PassChangingModalComponent
  ],
  imports: [
    BrowserModule,
    AngularFireDatabaseModule,
    AngularFontAwesomeModule,
    AngularFireAuthModule,
    NgbModule,
    BsDatepickerModule.forRoot(),
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyA4i7cg_Zw_0XNKqGdW66Im6JY-QtiU1sw",
      authDomain: "post-fc7a1.firebaseapp.com",
      databaseURL: "https://post-fc7a1.firebaseio.com",
      projectId: "post-fc7a1",
      storageBucket: "post-fc7a1.appspot.com",
      messagingSenderId: "927623470611"
    }),
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule
  ],
  providers: [
    PostAddService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
