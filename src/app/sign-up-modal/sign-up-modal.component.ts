import { Component, OnInit } from '@angular/core';
import { PostAddService } from '../post-add.service';

@Component({
  selector: 'app-sign-up-modal',
  templateUrl: './sign-up-modal.component.html',
  styleUrls: ['./sign-up-modal.component.css']
})
export class SignUpModalComponent implements OnInit {

  passwordToAsteriks;
  constructor( private s: PostAddService) { }

  ngOnInit() {
  }

  createU(v) {
    this.s.signUpUser(v.email, v.password);
    this.s.userPassword = v.password;
    this.passwordToAsteriks = this.s.userPassword.split("");
    for(let i=0; i<this.s.userPassword.length; i++){
      this.passwordToAsteriks[i] = "*";
    }
    this.passwordToAsteriks = this.passwordToAsteriks.join("");
    this.s.userPassword = this.passwordToAsteriks;
    this.s.flag = true;
  }
}
