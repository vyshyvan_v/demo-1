import { TestBed } from '@angular/core/testing';

import { PostAddService } from './post-add.service';

describe('PostAddService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PostAddService = TestBed.get(PostAddService);
    expect(service).toBeTruthy();
  });
});
