import { Component } from '@angular/core';
import { PostAddService } from './post-add.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'MyProject';

  constructor(private service: PostAddService, private router:Router){
    this.router.navigateByUrl('/');
  }
}
