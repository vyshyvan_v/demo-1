import { Component, OnInit, ViewChild, ElementRef,  AfterViewInit} from '@angular/core';
import { PostAddService } from '../post-add.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { post } from 'selenium-webdriver/http';


@Component({
  selector: 'app-create-post-modal',
  templateUrl: './create-post-modal.component.html',
  styleUrls: ['./create-post-modal.component.css']
})
export class CreatePostModalComponent implements AfterViewInit, OnInit {

  date;
  dd;
  mm;
  year;
  minDate: NgbDateStruct;
  flagForSelect = false;

  @ViewChild('expData') expDate: ElementRef;

  constructor(private http: PostAddService) { 
    let today: any = new Date();
    let dd: any = today.getDate() + 1;
    let mm: any = today.getMonth() + 1;
    let year: any = today.getFullYear();
   this.dd = dd;
   this.mm = mm;
   this.year = year;
   this.minDate = {year:year, month:mm, day:dd};
   dd = today.getDate();
   if (dd < 10) {
    dd = '0' + dd;
   }
   if ( mm < 10) {
    mm = '0' + mm
   }
   this.date = dd + '.' + mm + '.' + year;
  }

  ngAfterViewInit() {
  }

  ngOnInit(){
  }
  
  onSelectClick(){
    this.flagForSelect = true;
  }

  createPost(v) {
    this.http.sendPost(v);
    this.http.swModal = false;
  }

  ngOnDestroy(): void {
    console.log("Modal was destroyed");  
  }


}
