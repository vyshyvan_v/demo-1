import { Component, OnInit, AfterViewInit } from '@angular/core';
import { PostAddService } from '../post-add.service';

@Component({
  selector: 'app-sing-in-modal',
  templateUrl: './sing-in-modal.component.html',
  styleUrls: ['./sing-in-modal.component.css']
})
export class SingInModalComponent implements OnInit, AfterViewInit {

  passwordToAsteriks;
  constructor(private http: PostAddService) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  signIn(v) {
    this.http.signInUser(v.email, v.password);
    this.http.userPassword = v.password;
    this.passwordToAsteriks = this.http.userPassword.split("");
    for(let i=0; i<this.http.userPassword.length; i++){
      this.passwordToAsteriks[i] = "*";
    }
    this.passwordToAsteriks = this.passwordToAsteriks.join("");
    this.http.userPassword = this.passwordToAsteriks;
    this.http.flag = true;
  }

  ngOnDestroy(): void {
    console.log("Modal was destroyed");  
  }
}
