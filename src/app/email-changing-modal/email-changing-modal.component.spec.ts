import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailChangingModalComponent } from './email-changing-modal.component';

describe('EmailChangingModalComponent', () => {
  let component: EmailChangingModalComponent;
  let fixture: ComponentFixture<EmailChangingModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailChangingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailChangingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
