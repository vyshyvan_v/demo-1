import { Component, OnInit } from '@angular/core';
import { PostAddService } from '../post-add.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-email-changing-modal',
  templateUrl: './email-changing-modal.component.html',
  styleUrls: ['./email-changing-modal.component.css']
})
export class EmailChangingModalComponent implements OnInit {

  showModal(){
    this.http.swModal = true;
  }

  constructor(private http: PostAddService) { }

  ngOnInit() {
  }

  changeEmailMethod(v) {
    this.http.userEmail = v.new_email;
    this.changeEmail();
    this.http.swModal1 = false;
  }

  changeEmail() {
    var user = firebase.auth().currentUser;
    user.updateEmail(this.http.userEmail);
  }
  
  ngOnDestroy(): void {
    console.log("Modal was destroyed");  
  }
}
