import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SignedInPostListComponent } from "./signed-in-post-list/signed-in-post-list.component"
import { PostListComponent } from './post-list/post-list.component';

const routes = [
  { path: 'registered', component: SignedInPostListComponent },
  { path: '', component: PostListComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
