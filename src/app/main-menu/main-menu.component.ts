import { Component, OnInit, ViewChild } from '@angular/core';
import { PostAddService } from '../post-add.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.css']
})
export class MainMenuComponent implements OnInit {

 btn = false;
 date;
 dd;
 mm;
 year;
 dateValue;
 flagForSelect = false;
 flag = false;

 constructor(private http: PostAddService) { }

  ngOnInit() {
  }

  onDateInput($event) {
    let v = $event.target.value;
    this.dateValue = v.split('-');
    for (let i = 1; i <= 9; i++) {
      if (this.dateValue[2]==i) {
        this.dateValue[2] = '0' + i;
      }
    }
    console.log(this.dateValue);
    let year: any = this.dateValue[0];
    let mm: any = this.dateValue[1];
    let dd: any = this.dateValue[2];
    this.dd = dd;
    this.mm = mm;
    this.year = year;
    this.date = dd + '.' + mm + '.' + year;
    console.log(this.date);
    if (this.dateValue[0]!=undefined && this.dateValue[1]!=undefined && this.dateValue[2]!=undefined){
      this.http.changeDateValues(this.date);
    } else if (v==='' || (this.dateValue[0]==undefined || this.dateValue[1]==undefined || this.dateValue[2]==undefined)){
      this.http.getPosts();
    }
  }

  myFunction($event) {
    let v = $event.value;
    console.log(v);
    let year: any = v.year;
    let mm: any = v.month;
    let dd: any = v.day;
    this.dd = dd;
    this.mm = mm;
    this.year = year;
    if (dd < 10) {
      dd = '0' + dd;
    }
    if (mm < 10) {
      mm = '0' + mm;
     }
    this.date = dd + '.' + mm + '.' + year;
    console.log(this.date);
    if ((v.year!=undefined && v.year!='') && (v.month!=undefined && v.month!='') && (v.day!=undefined && v.day!='')){
      this.http.changeDateValues(this.date);
    }
    this.onSearch();
  }

  onSearch(){
    this.http.searchFlag = true;
  }

  onKey($event) {
    let v = $event.target.value;
    this.http.changeValues(v);
  }

  onLocationClick($event) {
    let v = $event.target.value;
    this.http.changeSelectLocationValues(v);
  }

  onCategoryClick($event) {
    let v = $event.value;
    this.http.changeSelectCategoryValues(v);
    this.flagForSelect = true;
  }

  onCheck(v){
    this.http.changecheckboxValues(v);
    console.log(v);
  }

  btnTextChange(){
    if(this.flag){
      this.flag = false;
    } else if (!this.flag){
      this.flag = true;
    }
  }

  showModal(){
    this.http.swModal = true;
    this.btn = true;
  }

}
