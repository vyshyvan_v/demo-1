import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignedInPostListComponent } from './signed-in-post-list.component';

describe('SignedInPostListComponent', () => {
  let component: SignedInPostListComponent;
  let fixture: ComponentFixture<SignedInPostListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignedInPostListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignedInPostListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
