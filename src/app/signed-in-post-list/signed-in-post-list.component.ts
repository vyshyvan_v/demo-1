import { Component, OnInit, DoCheck } from '@angular/core';
import { PostAddService } from '../post-add.service';

@Component({
  selector: 'app-signed-in-post-list',
  templateUrl: './signed-in-post-list.component.html',
  styleUrls: ['./signed-in-post-list.component.css']
})
export class SignedInPostListComponent implements OnInit, DoCheck {

  posts;
  values: string;
  selectlocationvalues: string;
  showBtn = true;
  copyPosts;
  counter = 0;

  constructor(private http: PostAddService) { }
  
  ngOnInit() {
    setTimeout(() => {
      this.http.getPosts().subscribe(p => {
        return this.posts = p.filter(el => {
          return el.email.includes(this.http.userEmail)
        })
      });  
      this.searchAll();
    } ,2000)
  }

  ngDoCheck(){
    if (this.http.searchFlag == true){
      this.searchAll();
      this.http.searchFlag = false;
    }
  }

  showMore() {
    this.http.postCounter +=5;
    this.http.getPosts().subscribe(p => {
      if (p.length < this.http.postCounter){
        this.showBtn = false;
      }
      return this.posts = p.filter(el => {
        return el.email.includes(this.http.userEmail);
      })
    }); 
  }

  searchAll(){
      if (this.counter == 0){
        this.copyPosts = this.posts;
        console.log(this.copyPosts);
        this.counter++;
      }

      this.http.currentvalues.subscribe(v =>{
        this.onSearch(v);
      });
      
      this.http.currentselectlocationvalues.subscribe(v =>{
        this.onLocationSelect(v);
      });
  
      this.http.currentselectcategoryvalues.subscribe(v =>{
        this.onCategorySelect(v);
      });
  
      this.http.currentdatevalues.subscribe(v =>{
        this.onDateInput(v);
      });
  
      this.http.currentcheckboxvalues.subscribe(v =>{
        this.onCheckbox(v);
      });

      this.posts = this.copyPosts;
  };

  onCheckbox(checkboxValue){
    if(checkboxValue){
          this.posts = this.posts.sort((a, b) => {
            if (a.date < b.date) {
              return 1;
            }
            if (a.date > b.date) {
              return -1;
            }
            return 0;
            })
        } else {
          this.posts = this.posts.sort((a, b) => {
            if (a.date > b.date) {
              return 1;
            }
            if (a.date < b.date) {
              return -1;
            }
            return 0;
            })
      }
  }

  onSearch(searchInputValue) {
    if (searchInputValue === '') {
        return this.posts;
    } else {
          this.posts = this.posts.filter(e => (e.describe.includes(searchInputValue) || e.description.includes(searchInputValue)))
      }
  }

  onDateInput(dateInputValue) {
    if (dateInputValue === '') {
        return this.posts;
    } else {
          this.posts = this.posts.filter(e => (e.date.includes(dateInputValue)))
      }
  }

  onLocationSelect(selectLocationValue) {
    if (selectLocationValue === '') {
      return this.posts;
    } else {
          this.posts = this.posts.filter(e => (e.location.includes(selectLocationValue)))
      }
  }

  onCategorySelect(selectCategoryValue) {
    if (selectCategoryValue === 'All' || selectCategoryValue === '') {
      return this.posts;
    } else {
          this.posts = this.posts.filter(e => (e.category.includes(selectCategoryValue)))
      } 
  }

  deletePost(k) {
    this.http.deletePost(k);
  }

  photoError(event){
    event.target.src = "https://cdn.browshot.com/static/images/not-found.png";
  }

}
