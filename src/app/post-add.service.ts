import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireList } from 'angularfire2/database';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';
import { HttpClient, HttpHeaders } from '@angular/common/http';

interface postObject {
  category: string,
  date: string,
  describe: string,
  description: string,
  expDate: {day: string, month: string, year: string},
  image: string,
  location: string,
  key: string;
}

@Injectable({
  providedIn: 'root'
})
export class PostAddService {

  postRef: Observable<any[]>;
  posts: Observable<any[]>;

  userEmail = "";
  userPassword = "";
  swModal = false;
  swModal1 = false;
  swModal2 = false;
  flag = false;
  searchFlag = false;
  private values = new BehaviorSubject<string>("");
  private selectlocationvalues = new BehaviorSubject<string>("");
  private selectcategoryvalues = new BehaviorSubject<string>("");
  private dateValues = new BehaviorSubject<string>("");
  private checkboxValues = new BehaviorSubject<string>("");
  currentvalues = this.values.asObservable();
  currentdatevalues = this.dateValues.asObservable();
  currentcheckboxvalues = this.checkboxValues.asObservable();
  currentselectlocationvalues = this.selectlocationvalues.asObservable();
  currentselectcategoryvalues = this.selectcategoryvalues.asObservable();

  postCounter = 5;

  constructor(private http: AngularFireDatabase, private newHttp: HttpClient) {}

  signUpUser(email, password) {
    this.userEmail = '';
    firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(resp => {
      firebase.auth().currentUser.sendEmailVerification()
      .then(() => {
        console.log('check email');
      });
      this.userEmail = resp.user.email; console.log(this.userEmail);
    })
    .catch(e => {
    });
  }

  signInUser(email, password) {
   firebase.auth().signInWithEmailAndPassword(email, password)
   .then(resp => {
     console.log(resp);
     this.userEmail = resp.user.email; console.log(this.userEmail);
   }).catch(e => console.log(e.message));
  }

  changeValues(values: string){
    this.values.next(values);
  }

  changecheckboxValues(checkboxValues: string){
    this.checkboxValues.next(checkboxValues);
  }

  changeDateValues(dateValues: string){
    this.dateValues.next(dateValues);
  }

  changeSelectLocationValues(selectlocationvalues: string){
    this.selectlocationvalues.next(selectlocationvalues);
  }

  changeSelectCategoryValues(selectcategoryvalues: string){
    this.selectcategoryvalues.next(selectcategoryvalues);
  }

  getPosts() {
    this.postRef = this.http.list('/posts', ref => { 
      return ref.limitToLast(this.postCounter);
    }).snapshotChanges().pipe(
      map(changes => {
          return changes.map(c => ({
              key: c.payload.key, ...c.payload.val()
          }))
      }));
    return this.postRef;
  }

  sendPost(v) {
    return this.http.list('/posts')
      .push(v);
  }
    deletePost(i) {
    return this.http.list('/posts').remove(i.key);
  }
}
